import React from 'react';
import styled from "styled-components";
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const SocialsWrapper = styled.ul`
    margin: 0 55px 17px;
    width: 103px;
    display: flex;
    list-style: none;
`
const SocialsItem = styled.li`
    margin-right: 13px;
`
const SocialsLink = styled.a`
    text-decoration: none;
    color: #fff;
`

const Socials = () => {
    return(
        <SocialsWrapper>
            <SocialsItem>
                <SocialsLink href="#">
                    <InstagramIcon style={{fill: '#fff'}}/>
                </SocialsLink>
            </SocialsItem>
            <SocialsItem>
                <SocialsLink href="#">
                    <FacebookIcon style={{fill: '#fff'}}/>
                </SocialsLink>
            </SocialsItem>
            <SocialsItem>
                <SocialsLink href="#">
                    <LinkedInIcon style={{fill: '#fff'}}/>
                </SocialsLink>
            </SocialsItem>
        </SocialsWrapper>
    )
}

export default Socials