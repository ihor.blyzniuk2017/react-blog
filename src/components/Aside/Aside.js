import React from "react";
import styled from "styled-components";
import AsideContent from "./AsideContent/AsideContent";

const AsideBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 300px;
    height: 100vh;
    background-color: #202020;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1000;
    box-shadow: 5px 0 15px rgb(5 5 5 / 10%);
`
const AsideHeader = styled.div`
    width: 100%;
    height: 180px;
    background-color: #121212;
`

const Aside = () => {
    return (
        <AsideBlock>
            <AsideHeader><img src='/img/sidebar-header.jpg'  alt='header_img'/></AsideHeader>
            <AsideContent/>
        </AsideBlock>
    )
}

export default Aside