import React from "react";
import {Avatar, Typography} from "@material-ui/core";
import styled from "styled-components";
import Button from "@mui/material/Button";
import Socials from "../../Socials/Socials";
import { NavLink } from "react-router-dom";


const Wrapper = styled.div`
  margin-top: -50px;
  padding: 0 10px;
  width: 100%;
`
const BtnWrap = styled.div`
  display: block;
  margin: 0 auto;
  width: 250px;
  display: flex;
  justify-content: space-between;

`
const TextName = styled(Typography)`
  &&{
    text-align: center;
    color: white;
    font-size: 20px;
  }
`
const TextPosition = styled(Typography)`
  &&{
    margin-bottom: 15px;
    text-align: center;
    color: #dedede;
    font-size: 14px;
    font-weight: 300;
  }
`

const TextDescr = styled(Typography)`
  &&{
    text-align: center;
    color: #dedede;
    font-size: 14px;
    padding: 20px 20px;
    text-align: center;
  }
`
const Ava = styled(Avatar)`
  &&{
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100px;
    height: 100px;
    display: block;
    margin: 0 auto 15px;
  }
`

const AsideContent = () => {
  return (
      <Wrapper>
        <Ava/>
        <TextName>Ihor</TextName>
        <TextPosition>Front-end developer</TextPosition>
        <Socials/>
        <TextDescr style={{textAlign: "center", color: 'whitesmoke', fontSize: '15px'}}>Front-end разработчик. Практик верстки сайтов. Созданием сайтов занимаюсь с 2012 года. Работал в нескольких ИТ компаниях и наработал более 10 000 часов в создании сайтов различной сложности.</TextDescr>

        <BtnWrap>
          <Button style={{backgroundColor: '#ED3024'}} variant="contained">
            <NavLink style={{textDecoration: 'none', color: '#fff'}} to="/works">My works</NavLink>
          </Button>
          <Button style={{backgroundColor: '#2A5FE7'}} variant="contained">Contact me</Button>
        </BtnWrap>

      </Wrapper>

  )
}

export default AsideContent