import * as React from 'react';
import { NavLink } from 'react-router-dom';

import styled from "styled-components";




const HeaderWrapper = styled.header`
    display: flex;
    width: calc(100% - 300px);
    position: fixed;
    top: 0;
    right: 0;
    z-index: 1000;
    background-color: #0d0d0d;
`

const HeaderLeft = styled.div`
    width: 60%;
`
const HeaderRight = styled.div`
    width: 40%;
    display: flex;
    justify-content: flex-end;
`
const NavList = styled.ul`
    display: flex;
    list-style: none;
`

const ListItem = styled.li`
    color: #fff;
    font-size: 12px;
    margin-right: 20px;
    text-transform: uppercase;

`
const Link = styled.a`
    color: #fff;
    text-decoration: none;
`
const Search = styled.form`
    width: 100%;
    max-width: 21rem;
    background-color: #202020;
`

const SearchInput = styled.input`
    display: block;
    width: 100%;
    padding: 15px 15px;
    background: transparent;
    border: none;
    font-family: inherit;
    font-size: 13px;
    color: #fff;
`

const Header = () => {
    return (
        <HeaderWrapper>
            <HeaderLeft>
                <nav>
                    <NavList>
                        <ListItem>
                            <NavLink to="/">
                                <Link>Main</Link>
                            </NavLink>
                        </ListItem>
                        <ListItem>
                            <NavLink to="/">
                                <Link>Articles</Link>
                            </NavLink>
                        </ListItem>
                        <ListItem>
                            <NavLink to="/">
                                <Link>About me</Link>
                            </NavLink>
                        </ListItem>
                    </NavList>
                </nav>
            </HeaderLeft>
            <HeaderRight>
                <nav>
                    <NavList>
                        <ListItem>
                            <NavLink to="/login">
                                <Link>Login</Link>
                            </NavLink>
                        </ListItem>
                    </NavList>
                </nav>
                <Search>
                    <SearchInput placeholder='Search'></SearchInput>
                </Search>
            </HeaderRight>
        </HeaderWrapper>

    );
}
export default Header