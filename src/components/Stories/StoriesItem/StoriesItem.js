import React from "react";
import styled from "styled-components"

const StoriesItemWrapper = styled.div`
    position: relative;
    overflow: hidden;
    border-radius: 5px;
    box-shadow: 0 5px 15px rgb(0 0 0 / 20%);
    cursor: pointer;
    ::after {
        content: '';
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.8));
        z-index: 2;
        }

`
const StoriesImg = styled.img`
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
    transition: transform 1s ease-in-out;
    :hover {
        transform: scale(1.3);
        transition-duration: 4s;
    }
`
const StoriesTitle = styled.div`
    width: 100%;
    padding: 14px;
    font-size: 12px;
    line-height: 1.3;
    color: #fff;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 3;
`
const StoriesTime = styled.time`
    width: 100%;
    padding: 14px;
    font-size: 12px;
    line-height: 1.3;
    color: #fff;
    text-align: right;
    position: absolute;
    bottom: 0;
    right: 0;
    z-index: 3;
`


const StoriesItem = (props) => {
    return (
        <StoriesItemWrapper>
            <StoriesImg src={props.imgUrl}/>
            <StoriesTitle>{props.text}</StoriesTitle>
            <StoriesTime>{props.time}</StoriesTime>
        </StoriesItemWrapper>
    )
}

export default StoriesItem