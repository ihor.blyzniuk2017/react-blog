import React from "react";
import styled from "styled-components"
import StoriesItem from "./StoriesItem/StoriesItem";

const StoriesWrapper = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    column-gap: 15px;
    grid-template-rows: 195px;
    margin-bottom: 30px;
`

const Stories = () => {
    return (
        <StoriesWrapper>
            <StoriesItem
                imgUrl='/img/stories1.jpg'
                text='Having a rest'
                time='2020-09-21'
            />
            <StoriesItem
                imgUrl='/img/stories2.jpg'
                text='Finishing project'
                time='2020-09-21'
            />
            <StoriesItem
                imgUrl='/img/stories3.jpg'
                text='Moved to new flat'
                time='2020-09-21'
            />
            <StoriesItem
                imgUrl='/img/stories4.jpg'
                text='Autumn is comming'
                time='2020-09-21'
            />
        </StoriesWrapper>
    )
}

export default Stories;