import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components"
import { addPost } from "../../Store/reducers/postsReducer";


const AddPostWrapper = styled.div`
    margin-bottom: 30px;
    padding: 15px 20px;
    box-shadow: 0 15px 25px rgb(0 0 0 / 45%);
    background-color: #121212;
    border-radius: 5px;
`
const AddPostForm = styled.div`
    display: flex;
    align-items: center;
    margin: 0;
`
const AddPostTextarea = styled.textarea`
    display: block;
    flex-grow: 1;
    height: 26px;
    font-size: 17px;
    color: #fff;
    font-family: inherit;
    border: none;
    background: transparent;
    resize: none;
    overflow: hidden;
    outline: none;
`
const AddPostActions = styled.div`
    display: flex;
    justify-content: space-between;
    width: 120px;
    flex-shrink: 0;
`
const Label = styled.label`
    display: block;
    width: 44px;
    height: 44px;
    border-radius: 50%;
    background: #ebebeb url(../img/photo-icon.svg) center no-repeat;
    cursor: pointer;
    border: none;
    transition: background .4s;
`
const AddPostBtn = styled.button`
    display: block;
    width: 44px;
    height: 44px;
    border-radius: 50%;
    background: #3137C9 url(/img/post-arrow.svg) center no-repeat;
    cursor: pointer;
    border: none;
    transition: background .4s;
`


const AddPost = () => {
    const dispatch = useDispatch()
    const [text, setText] = useState('')



    return (
        <AddPostWrapper>
            <AddPostForm>
                <AddPostTextarea value={text}  onChange={(e) => setText(e.target.value)} placeholder='write something...'/>
                <AddPostActions>
                    <Label>
                        <input style={{display: 'none'}} type='file'/>
                    </Label>
                    <AddPostBtn onClick={() => dispatch(addPost(text))} type='submit'/>
                </AddPostActions>
            </AddPostForm>
        </AddPostWrapper>
    )
}

export default AddPost