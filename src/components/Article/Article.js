import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components"

const Post = styled.div`
    margin-bottom: 30px;
    padding: 25px;
    border-radius: 5px;
    overflow: hidden;
    background-color: #202020;
    box-shadow: 0 15px 25px rgb(0 0 0 / 35%);
`
const PostContent = styled.div`
    margin-bottom: 20px;
`
const PostDescr = styled.div`
    font-size: 14px;
    color: #d2d2d2;
`
const PostFooter = styled.div`
    display: flex;
    justify-content: space-between;
`
const PostData = styled.ul`
    margin: 0;
    padding: 0;
    display: flex;
    color: #828282;
    list-style: none;
`
// const articleData = [
//     {
//         label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.'
//     },
//     {
//         label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.'
//     },
//     {
//         label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.'
//     }

// ]


const Article = () => {
    const articleData = useSelector(state => state.posts.posts);



    const posts = articleData.map((post) => {
        const text = post.label

        return (
            <Post>
                <PostContent>
                    <PostDescr>{text}</PostDescr>
                </PostContent>
                <PostFooter>
                    <PostData>
                        <li>
                            <time>21.06.2020</time>
                        </li>
                    </PostData>
                </PostFooter>
            </Post>
        )
    })

    return posts

}

export default Article