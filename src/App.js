import React from "react";
import Layout from "./layout/Layout";
import styled from "styled-components";

const Application = styled.div`
    font-family: 'Roboto', sans-serif;
`

function App() {
    return (
        <Application>


            <Layout/>

        </Application>
    );
}

export default App;
