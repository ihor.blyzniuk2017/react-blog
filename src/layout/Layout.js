import React from "react";
import Aside from "../components/Aside/Aside";
import Main from "./Main/Main";
import styled from "styled-components";
import Header from "../components/Header/Header";
const AppBlock = styled.div`
    display: flex;
`
const Layout = () => {
    return (
           <AppBlock>
               <Aside/>
               <Header/>
               <Main/>
           </AppBlock>
    )
}

export default Layout