import React from "react";
import styled from "styled-components";
import {Route, Switch} from "react-router-dom"
import Home from "../../pages/Home/Home";
import Login from "../../pages/Login/Login";
import Works from "../../pages/Works/Works";

const ContentBox = styled.div`
    padding: 100px 0 80px 300px;
    height: 100vh;
    width: 100%;

`

const Container = styled.div`
    box-sizing: border-box;
    max-width: 700px;
    margin: 0 auto;
    padding-left: 50px;
    padding-right: 50px;
`

const Main = () => {
    return (
        <ContentBox>

            <Container>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/works" component={Works}/>
                </Switch>
            </Container>
        </ContentBox>
    )
}

export default Main