import React from "react";
import Stories from "../../components/Stories/Stories";
import AddPost from "../../components/AddPost/AddPost";
import Article from "../../components/Article/Article";

const Home = () => {
    return (
        <>
        <Stories/>
        <AddPost/>
        <Article/>
        </>
    )
}

export default Home;