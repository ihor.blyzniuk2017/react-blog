// import { createAction, createReducer } from "@reduxjs/toolkit";

import { createSlice } from "@reduxjs/toolkit";

const postsSlice = createSlice({
    name: 'posts',
    initialState: {
        posts: [
            {
                id: 1,
                label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.',
                date: '15.06'
            },
            {
                id: 1,
                label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.',
                date: '15.06'
            },
            {
                id: 1,
                label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum volutpat orci turpis urna. Et vestibulum, posuere tortor lacinia sit. Sagittis porttitor orci auctor in at tincidunt arcu egestas. Fusce arcu sodales lacinia eu auctor nunc nam id. Diam sit sed volutpat massa. Egestas ornare vel volutpat.',
                date: '15.06'
            }
        ]
    },
    reducers: {

        addPost(state, action) {
            console.log(state)
            console.log(action.payload)
            state.posts.push({
                id: new Date().toISOString(),
                label: action.payload,
                date: '12.03'
            })
        }
    }
})

export default postsSlice.reducer

export const {addPost} = postsSlice.actions