import {combineReducers, configureStore} from '@reduxjs/toolkit';
import postsSlice from './reducers/postsReducer';

const rootReducer = combineReducers({
    posts: postsSlice
})

export const store = configureStore({
    reducer: rootReducer
})